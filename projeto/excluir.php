<?php include("../template/conexao.php"); ?>

<?php
    $msg_erro='';
    $msg_sucesso='';
    
    $id = $_POST['del-item-id'];

    /* teste se todos os campos foram passados no formularios*/
    if(isset($id) && $id)
    {
        /* teste se algum não foi preechidos*/
        if(empty($id))
            $msg_erro = "Código Inválido";

        $sql = "DELETE FROM `projeto` where CodProj = {$id}";

        /* executa o comando e retorna true se der tudo certo.*/
        /* Com Orientação a Objeto, invocando o método query do objeto conn*/
        $result = $conn->query($sql); 
        
        if ($result) { 
            $msg_sucesso = "Registro excluído com sucesso!";
        } else { 
            //echo $conn->error; die;
            //$msg_erro = "Erro: " . $sql . "<br>" . $conn->error;
            $msg_erro = "Não foi possível excluir.";
        } 

    // fecha ponto de conexão 
    //$conn->close(); 
        
    }    

?>