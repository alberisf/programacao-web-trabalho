<?php include("../template/cabecalho.php"); ?>

<?php include("../template/conexao.php"); ?>

<?php

    $codProj = $nome =  $cidade = '';

    if (isset($_GET['codproj']) && $_GET['codproj'])  {
        $codProj = $_GET['codproj'];

        $sql_proj = "SELECT * FROM projeto where codProj = " . $codProj;
        $resultado_proj = mysqli_query($conn, $sql_proj);
        $row_proj = mysqli_fetch_assoc($resultado_proj);

        $codProj = $row_proj['CodProj'];
        $nome    = $row_proj['Nome'];
        $cidade     = $row_proj['Cidade'];
    }

?>

   <!-- Form Examples area start-->
    <div class="form-example-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <form role="form" method="post" action="../projeto/salvar.php">
                        <div class="form-example-wrap mg-t-30">
                           <div class="cmp-tb-hd cmp-int-hd">
                            <h2>Formulário de Projetos</h2>
                        </div>

                        <!-- INPUT para nome-->   
                        <div class="form-example-int form-horizental">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                        <label class="hrzn-fm">Nome:</label>
                                    </div>
                                    <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                        <div class="nk-int-st">
                                            <input type="hidden" name="codproj" value="<?=$codProj?>">
                                            <input type="text" name="nome" value="<?=$nome?>" required="true" class="form-control input-sm" placeholder="Descrição do projeto">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- INPUT para cidade-->   
                       <div class="form-example-int form-horizental ">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                        <label class="hrzn-fm">Cidade:</label>
                                    </div>
                                    <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                        <div class="nk-int-st">
                                            <input type="text" name="cidade" value="<?=$cidade?>" class="form-control input-sm" placeholder="Cidade da projeto">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- botão submeter/salvar-->  
                        <div class="form-example-int mg-t-15">
                            <div class="row">
                                <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                </div>
                                <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                    <button class="btn btn-success notika-btn-success">Salvar</button>
                                </div>
                            </div>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Form Examples area End-->


<?php include("../template/rodape.php"); ?>

   