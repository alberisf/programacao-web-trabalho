<?php include("../template/cabecalho.php"); ?>

<?php include("../template/conexao.php"); ?>

<?php

$msg_erro='';
$msg_sucesso='';

/* teste se todos os campos foram passados no formularios*/
if(isset($_POST["nome"]) && isset($_POST["cidade"]) )
{
    /* teste se algum não foi preechidos*/
    if(empty($_POST["nome"]))
        $msg_erro = "Campo Nome obrigatório";
    else
    if(empty($_POST["cidade"]))
        $msg_erro = "Campo Cidade obrigatório";    

    else /* se tudo estiver ok, continua...*/
    {
        //Vamos realizar o cadastro ou alteração dos dados enviados.
        $nome   = $_POST["nome"];
        $cidade = $_POST["cidade"];

        if (isset($_POST['codproj']) && $_POST['codproj']) {
            $sql = "UPDATE projeto SET Nome = '{$nome}', Cidade = '{$cidade}' 
                        WHERE CodProj = {$_POST['codproj']}"; 
            $acao = 'atualizado';
        }
        else {
            $sql = "INSERT INTO `projeto` (`nome`,`cidade`) VALUES ('$nome','$cidade')";
            $acao = 'cadastrado';
        }
        
        /* executa o comando e retorna true se der tudo certo.*/
        /* Com Orientação a Objeto, invocando o método query do objeto conn*/
        $result = $conn->query($sql); 

        if ($result) { 
            $msg_sucesso = "Resgistro $acao com sucesso!"; 
        } else { 
            $msg_erro = "Erro: " . $sql . "<br>" . $conn->error;
    } 

// fecha ponto de conexão 
$conn->close(); 
        
    }
}    

?>

  
<!-- Alert area start-->
    <div class="alert-area">
        <div class="container">
               <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                         <!-- mostra o alert do tipo de mensagem. erro ou sucesso -->
                        <?php if(strlen($msg_erro) > 1) { ?>
                            <div class="alert alert-danger" role="alert"> <?php echo $msg_erro ?>       </div>
                        <?php } else{ ?>
                            <div class="alert alert-success" role="alert"> <?php echo $msg_sucesso ?>       </div>

                        <!-- se sucesso, mostra tambem os detalhes do cadastro -->
                       <div class="form-example-wrap mg-t-30">
                           <div class="cmp-tb-hd cmp-int-hd">
                            <h2>Detalhes do projeto</h2>
                            </div>
                        <div class="form-example-int form-horizental">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                        <label class="hrzn-fm">Nome:</label>
                                    </div>
                                    <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                        <div class="nk-int-st">
                                            <?php echo $nome ?>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                       <div class="form-example-int form-horizental ">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                        <label class="hrzn-fm">Cidade:</label>
                                    </div>
                                    <div class="col-lg-8 crow_projol-md-7 col-sm-7 col-xs-12">
                                        <div class="nk-int-st">
                                            <?php echo $cidade ?>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>  

                        <?php } ?>     

                    </div> 

                </div>
              <!-- botões volta e listagem: script da funcao voltar() no cabecalho.php -->
                <div class="btn-list">
                    <button onClick="location='../projeto/lista.php'" class="btn btn-primary notika-btn-primary">Listagem de Projeto</button>
                            
                    <?php if (!isset($_POST['codproj']) or !$_POST['codproj']) { ?>
                    <button onClick="location='../projeto/formulario.php'" class="btn btn-primary notika-btn-primary">Add Novo</button>
                    <?php } ?>
                </div>
        </div>
    </div>
    <!-- Alert area End-->




<?php include("../template/rodape.php"); ?>

   