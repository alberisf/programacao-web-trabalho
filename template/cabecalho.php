<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>SISTEMA WEB 1.0</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
        ============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
    <!-- Google Fonts
        ============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
        ============================================ -->
    <link rel="stylesheet" href="../template/css/bootstrap.min.css">
    <!-- font awesome CSS
        ============================================ -->
    <link rel="stylesheet" href="../template/css/font-awesome.min.css">
    <!-- owl.carousel CSS
        ============================================ -->
    <link rel="stylesheet" href="../template/css/owl.carousel.css">
    <link rel="stylesheet" href="../template/css/owl.theme.css">
    <link rel="stylesheet" href="../template/css/owl.transitions.css">
    <!-- meanmenu CSS
        ============================================ -->
    <link rel="stylesheet" href="../template/css/meanmenu/meanmenu.min.css">
    <!-- animate CSS
        ============================================ -->
    <link rel="stylesheet" href="../template/css/animate.css">
    <!-- normalize CSS
        ============================================ -->
    <link rel="stylesheet" href="../template/css/normalize.css">
    <!-- wave CSS
        ============================================ -->
    <link rel="stylesheet" href="../template/css/wave/waves.min.css">
    <link rel="stylesheet" href="../template/css/wave/button.css">
    <!-- mCustomScrollbar CSS
        ============================================ -->
    <link rel="stylesheet" href="../template/css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- Notika icon CSS
        ============================================ -->
    <link rel="stylesheet" href="../template/css/notika-custom-icon.css">
    <!-- Data Table JS
        ============================================ -->
    <link rel="stylesheet" href="../template/css/jquery.dataTables.min.css">
    <!-- main CSS
        ============================================ -->
    <link rel="stylesheet" href="../template/css/main.css">
    <!-- style CSS
        ============================================ -->
    <link rel="stylesheet" href="../template/css/style.css">
    <!-- responsive CSS
        ============================================ -->
    <link rel="stylesheet" href="../template/css/responsive.css">
    <!-- modernizr JS
        ============================================ -->
    <script src="template/js/vendor/modernizr-2.8.3.min.js"></script>

    <script src="../template/js/funcoes.js"></script>

    <script>
        function voltar() {
        window.history.back()
    }
    </script>
    
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- Start Header Top Area -->
    <div class="header-top-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="logo-area">
                        <a href="#"><img src="../template/img/logo/logo.png" alt="" /></a>
                    </div>
                </div>
              
            </div>
        </div>
    </div>
    <!-- End Header Top Area -->
    
  <?php include("menu.php"); ?>
  <hr>
