<?php include("../template/cabecalho.php"); ?>

<?php include("../template/conexao.php"); ?>

<?php

    $codPeca = $nome =  $cor = $peso = '';

    if (isset($_GET['codpeca']) && $_GET['codpeca'])  {
        $codPeca = $_GET['codpeca'];

        $sql_peca = "SELECT * FROM peca where CodPeca = " . $codPeca;
        $resultado_peca = mysqli_query($conn, $sql_peca);
        $row_peca = mysqli_fetch_assoc($resultado_peca);

        $codPeca = $row_peca['CodPeca'];
        $nome    = $row_peca['Nome'];
        $cor     = $row_peca['Cor'];
        $peso    = $row_peca['Peso'];
    }

?>


   <!-- Form Examples area start-->
    <div class="form-example-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <form role="form" method="post" action="../peca/salvar.php">
                        <div class="form-example-wrap mg-t-30">
                           <div class="cmp-tb-hd cmp-int-hd">
                            <h2>Formulário de Peças</h2>
                            </div>


                        <!-- INPUT para nome-->   
                        <div class="form-example-int form-horizental">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                        <label class="hrzn-fm">Nome:</label>
                                    </div>
                                    <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                        <div class="nk-int-st">
                                            <input type="hidden" name="codpeca" value="<?=$codPeca?>">
                                            <input type="text" name="nome" value="<?=$nome?>" required="true" class="form-control input-sm" placeholder="Descrição da peça">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                         <!-- INPUT para cor-->   
                        <div class="form-example-int form-horizental ">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                        <label class="hrzn-fm">Cor:</label>
                                    </div>
                                    <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                        <div class="nk-int-st">
                                            <input type="text" name="cor" value="<?=$cor?>" class="form-control input-sm" placeholder="Cor da peça">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <!-- INPUT para peso-->   
                       <div class="form-example-int form-horizental ">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                        <label class="hrzn-fm">Peso:</label>
                                    </div>
                                    <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                        <div class="nk-int-st">
                                            <input type="text" name="peso" value="<?=$peso?>" class="form-control input-sm" placeholder="Peso da peça">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <!-- botão submeter/salvar-->  
                        <div class="form-example-int mg-t-15">
                            <div class="row">
                                <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                </div>
                                <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                    <button class="btn btn-success notika-btn-success">Salvar</button>
                                </div>
                            </div>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Form Examples area End-->


<?php include("../template/rodape.php"); ?>

   