<?php include("../template/cabecalho.php"); ?>

<?php include("../template/conexao.php"); ?>


<?php
    $codFor = $_GET['codfor'];
    
    $sql_fornecedor = "SELECT * FROM fornecedor where CodFor = " . $codFor;

    $resultado_fornecedor = mysqli_query($conn, $sql_fornecedor);

    $row_forn = mysqli_fetch_assoc($resultado_fornecedor);

?>

   <!-- Form Examples area start-->
    <div class="form-example-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    
                        <div class="form-example-wrap mg-t-30">
                           <div class="cmp-tb-hd cmp-int-hd">
                            <h2>Detalhes de Fornecedor</h2>
                            </div>


                        <!-- INPUT para nome-->   
                        <div class="form-example-int form-horizental">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                        <label class="hrzn-fm">Nome:</label>
                                    </div>
                                    <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                        <div class="nk-int-st">
                                            <b><?= $row_forn['Nome']?></b>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                         <!-- INPUT para tipo-->   
                        <div class="form-example-int form-horizental ">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                        <label class="hrzn-fm">Tipo:</label>
                                    </div>
                                    <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                        <div class="nk-int-st">
                                            <b><?= $row_forn['Tipo']?></b>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <!-- INPUT para cidade-->   
                       <div class="form-example-int form-horizental ">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                                        <label class="hrzn-fm">Cidade:</label>
                                    </div>
                                    <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
                                        <div class="nk-int-st">
                                            <b><?= $row_forn['Cidade']?></b>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="btn-list">
                            <button onClick="location='../fornecedor/lista.php'" class="btn btn-primary notika-btn-primary">Voltar</button>
                        </div>                       
                    
                </div>
            </div>
        </div>
    </div>
    <!-- Form Examples area End-->


<?php include("../template/rodape.php"); ?>

   