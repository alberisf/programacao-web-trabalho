<?php include("../template/cabecalho.php"); ?>

<?php include("../template/conexao.php"); ?>

<?php

    $msg_erro = ''; 
    $msg_sucesso = '';

    if (isset($_POST['del-item-id']) && $_POST['del-item-id']) {
        include 'excluir.php';
    }

    $sql_fornecedor = "SELECT * FROM fornecedor ORDER BY nome";

    $resultado_fornecedor = mysqli_query($conn, $sql_fornecedor);

?>

   <!-- Data Table area Start-->
    <div class="data-table-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="data-table-list">

                        <?php if(strlen($msg_erro) > 1) { ?>
                            <div class="alert alert-danger" role="alert"> 
                                <?php echo $msg_erro ?>
                            </div>
                        <?php } 
                        if(strlen($msg_sucesso) > 1){ ?>
                            <div class="alert alert-success" role="alert"> 
                                <?php echo $msg_sucesso ?>       
                            </div>
                        <?php } ?>


                        <div class="basic-tb-hd">
                            <h2>Listagem de Fornecedores</h2>
                            <div class="btn-list">
                                <button onClick="location='../fornecedor/formulario.php'" class="btn btn-primary notika-btn-primary">Novo</button>
                            </div>
                            
                        </div>
                        <div class="table-responsive">

                            <form method="POST" action="lista.php">

                            <input type="hidden" name="del-item-id" id="del-item-id" value="">

                            <!-- Aqui começa a definição da tabela-->
                            <table id="data-table-basic" class="table table-striped">
                                <thead> <!-- Cabeçalho fixo-->
                                    <tr>
                                        <th>Cód</th>
                                        <th>Nome</th>
                                        <th>Tipo</th>
                                        <th>Cidade</th>
                                </thead> <!-- Fim do Cabeçalho fixo-->

                                <tbody>
									<?php // bloco de itereção no resultado
										while($row_fornecedor = mysqli_fetch_assoc($resultado_fornecedor))
										{
									?>
	                                    <tr> <!-- para cada registro repete esse bloco tr (linha) -->
											<td><?php echo $row_fornecedor['CodFor']; ?></td>
											<td><?php echo $row_fornecedor['Nome']; ?></td>
											<td><?php echo $row_fornecedor['Tipo']; ?></td>
											<td><?php echo $row_fornecedor['Cidade']; ?></td>
                                            <td>
                                                <a href="../fornecedor/detalhes.php?codfor=<?=$row_fornecedor['CodFor']?>"><span class="glyphicon glyphicon-list" title="Detalhes" aria-hidden="true"></span></a>
                                                <a href="../fornecedor/formulario.php?codfor=<?=$row_fornecedor['CodFor']?>"><span class="glyphicon glyphicon-edit" title="Editar" aria-hidden="true"></span></a>
                                            
                                                <!-- Button trigger modal -->
                                                <button type="button" onclick="set_delete_item(<?=$row_fornecedor['CodFor']?>)" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#myModal">
                                                    <span class="glyphicon glyphicon-trash" title="Excluir" aria-hidden="true"></span>
                                                </button>
                                            </td>
	                                    </tr>
                            		<?php } ?>
                                </tbody>

                                <tfoot>
                                    <tr>
                                        <th>Cód</th>
                                        <th>Nome</th>
                                        <th>Tipo</th>
                                        <th>Cidade</th>
                                    </tr>
                                </tfoot>

                            </table>

                            <!-- Modal -->
                                            
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Tem certeza?</h4>
                                            </div>
                                          
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                <button type="submit" class="btn btn-danger">Excluir</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                            <!-- /Modal -->

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Data Table area End-->



<?php include("../template/rodape.php"); ?>

   